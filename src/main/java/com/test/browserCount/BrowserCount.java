package com.test.browserCount;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

public class BrowserCount {

    /** Mapper implementation*/
    public static class TokenizerMapper
            extends Mapper<Object, Text, Text, IntWritable>
    {
    	private final static IntWritable writable = new IntWritable(1);
        private Text res = new Text();

        public void map(Object key, Text input, Context context)
        		throws IOException, InterruptedException 
        {
            String[] textRows = input.toString().split("\n");
            for (String line : textRows) 
            {
                String targetBrowser = browserFromString(line);

                /* if find browser failed we write error */
                if (targetBrowser == null)
                {
                    res.set("error");
                }
                else 
                {
                    res.set(targetBrowser);
                }

                context.write(res, writable);
            }
        }

        
        /**helper function to find browser name in string, return null if failed.*/
        public String browserFromString(String string)
        {
            if (string.isEmpty())
            {
                return null;
            }
            String[] splitRes = string.split(" ");
            if (splitRes.length < 2)
            {
            	//not enough fields
                return null;
            }
            
            //browser must be in the end of string
            String browserName = splitRes[splitRes.length - 1];
            if (browserName.toLowerCase().contains(("version")))
            {
                return null;
            }

            //delete "\" in the end
            if ((browserName.substring(browserName.length()-1).equalsIgnoreCase("\"")))
            {
            	browserName = browserName.replace(browserName.substring(browserName.length()-1), "");
            }

            return browserName;
        }
    }


    /** Reducer implementation */
    public static class IntSumReducer
            extends Reducer<Text,IntWritable,Text,IntWritable>
    {
        private IntWritable result = new IntWritable();

        public void reduce(Text key, Iterable<IntWritable> values, Context context)
                        		   throws IOException, InterruptedException
        {
            int sum = 0;
            //calculate same browsers using count
            for (IntWritable val : values)
            {
                sum += val.get();
            }
            result.set(sum);
            context.write(key, result);
        }
    }

    /** Configure job. */
    public static void main(String[] args) throws Exception
    {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "browser count");
        job.setJarByClass(BrowserCount.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        job.setNumReduceTasks(2);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        SequenceFileOutputFormat.setOutputPath( job, new Path(args[1]) );
        SequenceFileOutputFormat.setCompressOutput( job, true );
        SequenceFileOutputFormat.setOutputCompressorClass( job, SnappyCodec.class );

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
