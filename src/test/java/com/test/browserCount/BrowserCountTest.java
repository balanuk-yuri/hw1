package com.test.browserCount;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.Text;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import java.util.ArrayList;

/**
 * Unit test for browserTest.
 */
public class BrowserCountTest
    extends TestCase
{
	/**MapDriver and ReduceDriver are classes of mrunit for easy testing of mapper/reducer implementations */
    MapDriver<Object, Text, Text, IntWritable> mapDriver;
    com.test.browserCount.BrowserCount.TokenizerMapper mapper;
    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
    com.test.browserCount.BrowserCount.IntSumReducer reducer;

    public void setUp()
    {
        mapper = new com.test.browserCount.BrowserCount.TokenizerMapper();
        mapDriver = MapDriver.newMapDriver(mapper);

        reducer = new com.test.browserCount.BrowserCount.IntSumReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public BrowserCountTest( String testName )
    {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite(BrowserCountTest.class);
    }

    public void testMapPart() throws Exception
    {
        String inputString = "219.7.188.147 - - [11/Nov/2017:12:31:42 +0300] \"GET /list HTTP/1.0\" 200 5053 \"http://perkins-roberts.biz/list/tag/faq.php\" \"Mozilla/5.0 (Windows CE; bn-BD; rv:1.9.1.20) Gecko/2013-03-19 09:47:42 Firefox/3.6.9";
        mapDriver.withInput(new LongWritable(),new Text(inputString));
        mapDriver.withOutput(new Text("Firefox/3.6.9"), new IntWritable(1));
        mapDriver.runTest();
    }

    public void testGetBrouserFromEmptyString() throws Exception
    {
        String result = mapper.browserFromString("");
        assertEquals(null,result);
    }

    public void testGetBrouserFromStringWithOneWord() throws Exception
    {
        String result = mapper.browserFromString("abs");
        assertEquals(null,result);
    }

    public void testGetBrouserFromStringWithVersion() throws Exception
    {
        String result = mapper.browserFromString("zxcv version");
        assertEquals(null,result);
    }

    public void testGetBrouserFromNormalString() throws Exception
    {
        String result = mapper.browserFromString("Gecko/2013-03-19 09:47:42 Firefox/3.6.9");
        assertEquals("Firefox/3.6.9",result);
    }

    public void testGetBrouserFromNormalStringWithFinalBracket() throws Exception
    {
        String result = mapper.browserFromString("Gecko/2013-03-19 09:47:42 Firefox/3.6.9\"");
        assertEquals("Firefox/3.6.9",result);
    }

    public void testReducePart() throws Exception
    {
        ArrayList<IntWritable> array = new ArrayList<IntWritable>();
        array.add(new IntWritable(1));
        reduceDriver.withInput(new Text("Firefox/3.6.9"), array);
        
        array.add(new IntWritable(1));
        reduceDriver.withInput(new Text("Chrome"), array);

        reduceDriver.withOutput(new Text("Firefox/3.6.9"), new IntWritable(1));
        reduceDriver.withOutput(new Text("Chrome"), new IntWritable(2));

        reduceDriver.runTest();
    }

}
