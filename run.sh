hadoop fs -rm -r /test/Out
echo "Cleaning out dir..."
hadoop jar target/browsercount-1.0-SNAPSHOT.jar com/test/browserCount/BrowserCount /test/In/ /test/Out/
echo "Hadoop finished work"
echo "Cleaning out file..."
rm -f ./outputFile
hadoop fs -text /test/Out/part* > ./outputFile
echo "Results place in ./outputFile"
